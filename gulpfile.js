'use strict'
const { src,dest } = require('gulp');
var gulp = require('gulp');
var   browserSync=require('browser-sync').create();
var   sass=require('gulp-sass');




function style(){
    return gulp.src('./css/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
}
function watch(){
    browserSync.init({
        server:{
            baseDir:"./",
            index:"index.html"
        }
    });
    gulp.watch('css/*.scss',style)
    gulp.watch('./*.html').on('change',browserSync.reload);
}

function copy(){
    return src('./*.html')
    .pipe(dest('distri/'));
}

exports.style=style;
exports.watch=watch;
exports.copy=copy;